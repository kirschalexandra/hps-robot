; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.utilities)

(defn limit-to-range [value minval maxval]
  "makes sure the given value lies between minval and maxval or returns the min/max value"
  (if (pos? value)
    (min value maxval)
    (max value minval)))

(defn distance-to-evaluation-linear [dist max-distance]
  ; y = m*x +t
  ; t = 1
  ; m = -1/max-distance
  (inc (* (/ -1 max-distance) dist)))


; Euklidsche Distanz für hashmaps
(defn hashmap-distance [m1 m2]
	(math/sqrt (reduce + (map (comp #(math/expt % 2) -)
    												(vals (into (sorted-map) m1))
    												(vals (into (sorted-map) m2))))))

(defn limit-proposed-range [proposed-map min-values-map max-values-map]
  (into {} (map (fn [[proposed-key proposed-val] minval maxval]
                  [proposed-key
                   (cond (< proposed-val minval) minval
                         (> proposed-val maxval) maxval
                         :else proposed-val)])
                proposed-map
                (vals min-values-map)
                (vals max-values-map))))

(defn compare-to-ideal
	([proposed-map ideal-map min-values-map max-values-map]
	 (compare-to-ideal proposed-map ideal-map min-values-map max-values-map distance-to-evaluation-linear))
	([proposed-map ideal-map min-values-map max-values-map interpolation-function]
	 (let [chunk-dist-to-ideal (hashmap-distance ideal-map (limit-proposed-range proposed-map min-values-map max-values-map))
         min-dist-to-ideal   (hashmap-distance ideal-map max-values-map)
         max-dist-to-ideal   (hashmap-distance ideal-map min-values-map)]
    (limit-to-range
      (interpolation-function
        chunk-dist-to-ideal
        (max min-dist-to-ideal max-dist-to-ideal))
      0.0
      1.0))))

