; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns robot.environment
  (:require [robot.utilities :as util])
  (:use [clojure.pprint])
  (:load "environment/generic" "environment/topological-map" "environment/indoors" "environment/apartment" "environment/lph" "environment/test-environments"))

;;; Furniture objects:
;;; There does not seem to be any (straightforward) way to extract the objects size at runtime from blender

;;; for each object, we store the left corner towards someone standing in front of the object
;;; it is advised to access object coordinates with the functions get-global-coordinate-limits or get-drawing-coordinates

;;; all measures are in meters
;;; angles (rotation) are in degrees, conversion to radians (yaw) with function get-yaw

;;; doors and drawers:
;; doors and drawers are represented as objects on their own, references by the parent furniture object
;; to make calls to opening and closing more convenient, the names of the doors and drawers are the same as the labels in Blender
;; thus, we don't have to store any additional label and don't need to access the door/drawer object when manipulating it
;; the door/drawer objects are mostly used for the state information
