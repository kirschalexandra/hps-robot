; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.environment)

; Verbindung von p0 nach p1 als Bezier-Kurve:
; C(t) = (1-t)P0 + tP1 (Formel aus Wikipedia)
; sampling-rate: Anzahl Punkte pro m
(defn points-connectable? [p0 p1 relevant-furniture & {:keys [sampling-rate] :or {sampling-rate 10}}]
  (let [diff (/ 1 (* (util/point-distance p0 p1) sampling-rate))]
    (loop [t diff]
      (if (>= t 1)
        true
        (let [testpoint {:x (+ (* (- 1 t) (:x p0)) (* t (:x p1)))
                         :y (+ (* (- 1 t) (:y p0)) (* t (:y p1)))}
              collisions (map #(util/point-in-rectangle testpoint (get-global-coordinate-limits %)) relevant-furniture)]
          (if (some #(= % true) collisions)
            false
            (recur (+ t diff))))))))

(defn get-obstacles-between-points [p0 p1 relevant-furniture & {:keys [sampling-rate] :or {sampling-rate 10}}]
  (let [diff (/ 1 (* (util/point-distance p0 p1) sampling-rate))]
    (loop [t diff
           all-collisions []]
      (if (>= t 1)
        all-collisions
        (let [testpoint {:x (+ (* (- 1 t) (:x p0)) (* t (:x p1)))
                         :y (+ (* (- 1 t) (:y p0)) (* t (:y p1)))}
              collisions (reduce (fn [collisions obstacle]
                                   (if (util/point-in-rectangle testpoint (get-global-coordinate-limits obstacle))
                                     (conj collisions (or (:label obstacle) :unknown-obstacle))
                                     collisions))
                                 []
                                 relevant-furniture)]
           (recur (+ t diff) (concat all-collisions collisions)))))))



(defn generate-topomap-connections [topo-points obstacles]
  (loop [topopoints topo-points
         connections []]
    (if (empty? topopoints)
      connections
      (recur (rest topopoints)
             (concat
               connections
               (remove nil? (let [[start-point-name start-point-value] (first topopoints)] (map (fn [[point-name point-value]] (when (points-connectable? start-point-value point-value obstacles) [start-point-name point-name])) (rest topopoints)))))))))


(defn generate-topological-map [points connections]
  (reduce (fn [topomap [p1 p2]]
            (update-in
              (update-in topomap [p1 :neighbours] conj p2)
              [p2 :neighbours] conj p1))
          points
          connections))

;; A* search through map
(defn plan-topomap-path [start-point-name goal-point-name topomap]
  (let [goal-point-data (get topomap goal-point-name)]
    (letfn [(make-node [node-name [parent-name {parent-dist-from-start :dist-from-start :as parent-data} :as parent]]
              (let [heuristic-value (util/point-distance (get topomap node-name) goal-point-data)
                    dist-from-start (if parent-name
                                      (+ parent-dist-from-start (util/point-distance (get topomap node-name) (get topomap parent-name)))
                                      0)]
                [node-name {:parent parent
                            :heuristic heuristic-value
                            :dist-from-start dist-from-start
                            :node-evaluation (+ dist-from-start heuristic-value)}]))
            (expand-node [parent-node]
              (map #(make-node % parent-node)
                    (:neighbours (get topomap (first parent-node)))))
            (add-to-queue [new-nodes queue visited-nodes]
              (sort-by (comp :node-evaluation second) >
                       (reduce (fn [qq nn] (if (some #(= % (first nn)) visited-nodes) qq (conj qq nn))) queue new-nodes)))
            (extract-path [node]
              (loop [path ()
                     [node-name node-data] node]
                (if node-name
                  (recur (conj path node-name) (:parent node-data))
                  path)))]
      (loop [nodes (vec (add-to-queue [(make-node start-point-name nil)] [] []))
             visited-nodes [start-point-name]]
        (when (seq nodes) (let [node (peek nodes)] (if (= (first node) goal-point-name) (extract-path node) (recur (vec (add-to-queue (expand-node node) (pop nodes) visited-nodes)) (conj visited-nodes (first node))))))))))

; Aufrufbeispiel:
; (plan-topomap-path :blue-block-side-1 :green-block-side-3 (:topomap boxes))

(defn get-closest-topomap-point
  "returns closest point on topological map, if min-dist is provided, the returned point must have at least this distance to point (thus, the return value may not be exactly the closest point)"
  ([pose topomap] (get-closest-topomap-point pose topomap 0.0))
  ([pose topomap min-dist]
   (loop [closest-point (key (first topomap))
          closest-dist (util/point-distance pose (val (first topomap)))
          [topopoint & toporest] (rest topomap)]
     (if (empty? topopoint)
       closest-point
       (let [dist (util/point-distance pose (val topopoint))]
         (if (and (< dist closest-dist) (>= dist min-dist))
           (recur (key topopoint) dist toporest)
           (recur closest-point closest-dist toporest)))))))


(defn get-closest-reachable-topomap-point
  "returns closest reachable point on topological map, if min-dist is provided, the returned point must have at least this distance to point (thus, the return value may not be exactly the closest point)"
  ([pose topomap relevant-furniture] (get-closest-reachable-topomap-point pose topomap relevant-furniture 0.0))
  ([pose topomap relevant-furniture min-dist]
   (when (first topomap)
     (loop [closest-point (key (first topomap))
            closest-dist (util/point-distance pose (val (first topomap)))
            [topopoint & toporest] (rest topomap)]
       (if (empty? topopoint)
         closest-point
         (let [dist (util/point-distance pose (val topopoint))]
           (if (and (< dist closest-dist) (>= dist min-dist) (points-connectable? pose (val topopoint) relevant-furniture))
             (recur (key topopoint) dist toporest)
             (recur closest-point closest-dist toporest))))))))


