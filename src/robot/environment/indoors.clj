; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.environment)

(defn append-keyword [kw appendstring]
  (keyword (str (name kw) appendstring)))

(def indoors-furniture {:red-block   {:pose {:x -8 :y -0.5 :rotation 0} :size {:width 1 :depth 1}}
                        :green-block {:pose {:x -7 :y  5.5 :rotation 0} :size {:width 1 :depth 1}}
                        :blue-block  {:pose {:x -4 :y -3.5 :rotation 0} :size {:width 1 :depth 1}}})

(def boxes-topomap-points
  (into {}
    (mapcat (fn [[blockname {pose :pose size :size}]]
              (let [dist-to-object 0.5
                    y-bottom (- (:y pose) dist-to-object)
                    y-middle (+ (:y pose) (* 0.5 (:depth size)))
                    y-top    (+ (:y pose) (:depth size) dist-to-object)
                    x-left   (- (:x pose) dist-to-object)
                    x-middle (+ (:x pose) (* 0.5 (:width size)))
                    x-right  (+ (:x pose) (:width size) dist-to-object)
                    append-block-name (partial append-keyword blockname)]
                (list
                  [(append-block-name "-side-1")   {:x x-middle :y y-bottom :neighbours #{}}]
                  [(append-block-name "-side-2")   {:x x-left   :y y-middle :neighbours #{}}]
                  [(append-block-name "-side-3")   {:x x-middle :y y-top    :neighbours #{}}]
                  [(append-block-name "-side-4")   {:x x-right  :y y-middle :neighbours #{}}]
                  [(append-block-name "-corner-1") {:x x-left   :y y-bottom :neighbours #{}}]
                  [(append-block-name "-corner-2") {:x x-left   :y y-top    :neighbours #{}}]
                  [(append-block-name "-corner-3") {:x x-right  :y y-top    :neighbours #{}}]
                  [(append-block-name "-corner-4") {:x x-right  :y y-bottom :neighbours #{}}])))
            indoors-furniture)))
            ; (:platform (:furniture boxes)))))


(def boxes-topomap-connections
     (generate-topomap-connections boxes-topomap-points (vals indoors-furniture)))
                                   ; (concat (mapcat vals (vals (:furniture boxes))))))

; der Name boxes muss so sein, weil das der Name ist, der von Morse zurückgeliefert wird, der intern verwendete Name steht in der Meta-Information
(def boxes #^{:name :indoors}
  {:rooms {:platform {:pose {:x -10.1 :y -10.1 :rotation 0} :size {:width 20.2 :depth 20.2}}}
   :outer-walls [{:pose {:x -10.1 :y  10   :rotation 0}  :size {:width 20.2 :depth 0.1} :adjacent-rooms [:platform] :label "top wall"}
                 {:pose {:x -10.1 :y -10.1 :rotation 0}  :size {:width 20.2 :depth 0.1} :adjacent-rooms [:platform] :label "bottom wall"}
                 {:pose {:x -10   :y -10   :rotation 90} :size {:width 20   :depth 0.1} :adjacent-rooms [:platform] :label "left wall"}
                 {:pose {:x  10.1 :y -10   :rotation 90} :size {:width 20   :depth 0.1} :adjacent-rooms [:platform] :label "right wall"}]
   :furniture {:platform indoors-furniture}
   :topomap (generate-topological-map boxes-topomap-points boxes-topomap-connections)})
