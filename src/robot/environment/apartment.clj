; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.environment)

(def kitchen-furniture
  {:stove
    {:pose {:x -3.1 :y -0.9 :rotation 90}
     :size {:width 1.0 :depth 1.0 :height 0.9}
     :doors '(oven-door)}
   :drawers-cupboard
    {:pose {:x -3.1 :y 0.1 :rotation 90}
     :size {:width 0.5 :depth 1.0 :height 0.9}
     :drawers '(kitchenet-drawer-top kitchenet-drawer-middle kitchenet-drawer-bottom)}
   :left-cupboard
    {:pose {:x -3.1 :y 0.6 :rotation 90}
     :size {:width 0.8 :depth 1.0 :height 0.9}
     :doors '(kitchenet-door-left)}
   :right-cupboard
    {:pose {:x -3.1 :y 1.4 :rotation 90}
     :size {:width 0.8 :depth 1.0 :height 0.9}
     :doors '(kitchenet-door-right)}
   :sink
    {:pose {:x -3.1 :y 2.2 :rotation 90}
     :size {:width 0.8 :depth 1.0 :height 0.9}
     :default-approach-pose {:x -2.6 :y 1.7 :yaw (* Math/PI 3/4)}}
   :dishwasher
    {:pose {:x -3.1 :y 2.2 :rotation 0}
     :size {:width 1.0 :depth 0.8 :height 0.9}}
   :kitchen-bar
    {:pose {:x -1.4 :y 1.65 :rotation 90}
     :size {:width 1.35 :depth 0.7 :height 0.9}
     :other-approach-poses [{:x -1.75 :y 1.25 :yaw 0} {:x -1.1 :y 1.25}]}
   :fridge
    {:pose {:x -0.5 :y -0.05 :rotation 180}
     :size {:width 1.08 :depth 0.86 :height 1.98}
     :doors '(refrigerator)}
   :kitchen-table
    {:pose {:x 0.5 :y 2.0 :rotation 0}
     :size {:width 1.569 :depth 0.98 :height 0.96}
     :other-approach-poses [{:x 0.1 :y 1.6 :yaw (/ Math/PI 4)}]}
   ; :wall-cupboard-left
   ;  {:pose {:x -3.3 :y 0.75 :z 1.75 :rotation 90}
   ;   :size {:width 0.55 :depth 0.8 :height 0.75}
   ;   :doors '(wall-cupboard-left)}
   ; :wall-cupboard-middle
   ;  {:pose {:x -3.3 :y 1.3 :z 1.75 :rotation 90}
   ;   :size {:width 0.55 :depth 0.8 :height 0.75}
   ;   :doors '(wall-cupboard-middle)}
   ; :wall-cupboard-right
   ;  {:pose {:x -3.3 :y 1.85 :z 1.75 :rotation 90}
   ;   :size {:width 0.55 :depth 0.8 :height 0.75}
   ;   :doors '(wall-cupboard-right)}
})

(def app-furniture {:kitchen kitchen-furniture
                    :livingroom {:couch-table       {:pose {:x 3.74   :y  1.549 :rotation -90} :size {:width 1.224 :depth 0.78}}
                                 :tv-board          {:pose {:x 3.0454 :y -0.226 :rotation  90} :size {:width 2.903 :depth 0.6956}}
                                 :living-room-shelf {:pose {:x 6.2699 :y -2.469 :rotation 180} :size {:width 2.4   :depth 0.6}}
                                 :couch-part-1      {:pose {:x 5.3    :y 0.6    :rotation   0} :size {:width 0.98  :depth 1.5}}
                                 :couch-part-2      {:pose {:x 4.4    :y 2.1    :rotation   0} :size {:width 1.88  :depth 0.89}
                                                     :default-approach-pose {:x 4.1, :y 2.545}}}
                    :bedroom {:bed          {:pose {:x 4.05     :y -3.999  :rotation -90} :size {:width 1.52    :depth 2.18}}
                              :desk         {:pose {:x 2.2647   :y -5.5035 :rotation 180} :size {:width 1.284   :depth 0.567}
                                             :default-approach-pose {:x 1.2, :y -5.2035}}
                              :office-chair {:pose {:x 1.54788  :y -5.475  :rotation   0} :size {:width 0.49824 :depth 0.55659}
                                             :default-approach-pose {:x 1.797, :y -4.66}}
                              :wardrobe     {:pose {:x -0.92775 :y -6.06345 :rotation 90} :size {:width 2.773   :depth 0.87225}}}})

(def app-outer-walls [{:pose {:x -6.5 :y  3.0  :rotation 0}  :size {:width 13   :depth 0.2} :adjacent-rooms [:bathroom :kitchen :livingroom]   :label "outer wall kitchen side"}
                      {:pose {:x -6.3 :y -6.1  :rotation 90} :size {:width 9.2  :depth 0.2} :adjacent-rooms [:bathroom :corridor :storageroom] :label "outer wall entrance side"}
                      {:pose {:x -6.5 :y -6.3  :rotation 0}  :size {:width 13   :depth 0.2} :adjacent-rooms [:storageroom :bedroom]            :label "outer wall bedroom side"}
                      {:pose {:x  6.5 :y -6.1  :rotation 90} :size {:width 9.2  :depth 0.2} :adjacent-rooms [:livingroom :bedroom]             :label "outer wall living room side"}])
(def app-inner-walls [{:pose {:x -4.1 :y -0.9  :rotation 90} :size {:width 4    :depth 0.2} :adjacent-rooms [:bathroom :kitchen]               :label "inner wall bathroom-kitchen"}
                      {:pose {:x -1.8 :y -6.1  :rotation 90} :size {:width 2.8  :depth 0.2} :adjacent-rooms [:bedroom :storageroom]            :label "inner wall bedroom-storageroom"}
                      {:pose {:x  2.3 :y -1.76 :rotation 90} :size {:width 4.86 :depth 0.2} :adjacent-rooms [:kitchen :livingroom :corridor]   :label "inner wall kitchen-livingroom"}
                      {:pose {:x  2.3 :y -3.1  :rotation 90} :size {:width 0.2  :depth 0.2} :adjacent-rooms [:corridor :livingroom]            :label "inner wall livingroom-corridor"}
                      {:pose {:x 1.94 :y -3.3  :rotation 0}  :size {:width 4.56 :depth 0.2} :adjacent-rooms [:bedroom :livingroom]             :label "inner wall livingroom-bedroom"}
                      {:pose {:x -6.3 :y -3.3  :rotation 0}  :size {:width 6.9  :depth 0.2} :adjacent-rooms [:storageroom :corridor]           :label "inner wall storageroom-corridor"}
                      {:pose {:x -6.3 :y -1.1  :rotation 0}  :size {:width 0.66 :depth 0.2} :adjacent-rooms [:bathroom :corridor]              :label "inner wall bathroom-corridor"}
                      {:pose {:x -4.5 :y -1.1  :rotation 0}  :size {:width 5.16 :depth 0.2} :adjacent-rooms [:kitchen :corridor]               :label "inner wall kitchen-corridor"}
                      {:pose {:x  1.9 :y -1.1  :rotation 0}  :size {:width 0.2  :depth 0.2} :adjacent-rooms [:kitchen :corridor]               :label "inner wall kitchen-corridor rest"}])

(def app-room-connections [[:corridor :kitchen     {:x 1.28  :y -1.0}]
                           [:corridor :bathroom    {:x -5.07 :y -1.0}]
                           [:corridor :livingroom  {:x 2.2   :y -2.33}]
                           [:corridor :bedroom     {:x 1.28  :y -3.2}]])

;;; Topological Map

;; Automatic specification of topological map
(defn topomap-points [furniture-map]
  (into {}
    (mapcat (fn [[furniture-name furniture-data]]
              (let [points (get-approach-poses furniture-data)]
                (map vector
                     (map #(keyword (str (name furniture-name) "-" %)) (range (count points))) ; names of topomap points
                     points)))
            furniture-map)))


(def apartment-topomap-points
  (into {}
        (concat (mapcat topomap-points (vals app-furniture))
                (map (fn [[room1 room2 doorpoint]] [(keyword (str "door-" (name room1) "-" (name room2))) doorpoint])
                     app-room-connections))))


(def apartment-topomap-connections
     (generate-topomap-connections apartment-topomap-points
                                   (concat (mapcat vals (vals app-furniture))
                                           app-inner-walls)))

;;; Environment definition

; der Name apartment-without-chair muss so sein, weil das der Name ist, der von Morse zurückgeliefert wird, der intern verwendete Name steht in der Meta-Information
(def apartment-without-chair #^{:name :apartment}
  {:rooms {:corridor     {:pose {:x -6.5 :y -3.3 :rotation 0} :size {:width 8.8 :depth 2.4}} ;immer Außenseiten der Wände rechnen, ist besser für Anzeige
           :kitchen      {:pose {:x -4.3 :y -1.1 :rotation 0} :size {:width 6.6 :depth 4.3}}
           :bathroom     {:pose {:x -6.5 :y -1.1 :rotation 0} :size {:width 2.4 :depth 4.3}}
           :livingroom   {:pose {:x  2.1 :y -3.3 :rotation 0} :size {:width 4.4 :depth 6.5}}
           :bedroom      {:pose {:x -2.0 :y -6.3 :rotation 0} :size {:width 8.5 :depth 3.2}}
           :storageroom  {:pose {:x -6.5 :y -6.3 :rotation 0} :size {:width 4.7 :depth 3.2}}}
   :room-connections app-room-connections
   :outer-walls app-outer-walls
   :inner-walls app-inner-walls
   :furniture app-furniture
   :topomap (generate-topological-map apartment-topomap-points apartment-topomap-connections)})

