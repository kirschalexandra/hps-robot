; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.environment)


(defn get-global-coordinate-limits [object]
  "provides coordinate limits as a vector [min-x max-x min-y max-y]"
  (let [front-left (:pose object)
        size (:size object)]
    (case (:rotation front-left)
      0   [(:x front-left)
           (+ (:x front-left) (:width size))
           (:y front-left)
           (+ (:y front-left) (:depth size))]
      90  [(- (:x front-left) (:depth size))
           (:x front-left)
           (:y front-left)
           (+ (:y front-left) (:width size))]
      180 [(- (:x front-left) (:width size))
           (:x front-left)
           (- (:y front-left) (:depth size))
           (:y front-left)]
      -90 [(:x front-left)
           (+ (:x front-left) (:depth size))
           (- (:y front-left) (:width size))
           (:y front-left)])))

(defn get-drawing-coordinates [object]
  "provides coordinate limits as a vector [min-x max-x width height], rotated to global coordinate system"
  (let [[min-x max-x min-y max-y] (get-global-coordinate-limits object)]
    [min-x min-y (- max-x min-x) (- max-y min-y)]))

(defn get-yaw [{rotation :rotation}]
  (util/degree-to-radian rotation))


;; Functions for calculating specific points: front-left, front, front-right, approach-pose
(defn front-left [object]
  (:pose object))

(defn front-middle-and-right [furniture-object scale-factor offset]
  (let [{x-fl :x y-fl :y} (front-left furniture-object)
        scaled-width (* scale-factor (:width (:size furniture-object)))]
    (case (:rotation (:pose furniture-object))
      0   {:x (+ x-fl scaled-width) :y (- y-fl offset)} ; stimmen Orientierungen???
      90  {:x (+ x-fl offset)       :y (+ y-fl scaled-width)}
      180 {:x (- x-fl scaled-width) :y (+ y-fl offset)}
      -90 {:x (- x-fl offset)       :y (- y-fl scaled-width)}
      (do (println "Rotation angle ~a for furniture not supported, returning front-left coordinate" (:rotation (:pose furniture-object)))
          (front-left furniture-object)))))

(defn front-right
  ([furniture-object] (front-right furniture-object 0))
  ([furniture-object offset]
   (front-middle-and-right furniture-object 1 offset)))

(defn front
  ([furniture-object] (front furniture-object 0))
  ([furniture-object offset]
   (front-middle-and-right furniture-object 0.5 offset)))


(defn get-approach-poses [furniture-object]
  (let [robot-furniture-default-distance 0.4]
    (conj (:other-approach-poses furniture-object)
          (or (:default-approach-pose furniture-object)
              (front furniture-object robot-furniture-default-distance)))))

