; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.navigation)

(def update-frequency* 60) ; Hz

(defn predict-pose-all [current-pose navcmd & {:keys [delta-time delta-steps]}]
  "predict next pose analytically from current pose and proposed velocity"
  ; delta-time in ms (wie überall sonst), wird umgerechnet in s (ist leichter zu rechnen)
  (let [delta-time-in-s (if (nil? delta-time)
                          (if (nil? delta-steps)
                            1.0 ;(/ 1 update-frequency*) ; default, wenn nichts angegeben wurde: 1 Zeitschritt
                            (/ delta-steps update-frequency*)) ; Umrechnung Zeitschritte in s
                          (do
                            (when delta-steps
                              (println "Function predict-pose-all called with input for delta-time and delta-steps. Ignoring delta-steps, using delta-time"))
                            (/ delta-time 1000))) ; Umrechnung Zeitangabe in s
        dist (* delta-time-in-s (math/sqrt (+ (math/expt (:x navcmd) 2) (math/expt (:y navcmd) 2))))
        movement-angle (+ (:yaw current-pose) (Math/atan2 (:y navcmd) (:x navcmd)))]
    [{:x (+ (:x current-pose) (* (Math/cos movement-angle) dist))
      :y (+ (:y current-pose) (* (Math/sin movement-angle) dist))
      :yaw (+ (:yaw current-pose) (* delta-time-in-s (:w navcmd)))}
     dist
     movement-angle]))

(defn predict-pose [& args]
  (first (apply predict-pose-all args)))

(defn check-pose-prediction []
  (let [test-commands [{:x 1 :y 0 :w 0}
                       {:x 0 :y 1 :w 0}
                       {:x 0 :y -1 :w 0}
                       {:x 0 :y 0 :w 1}
                       {:x 0 :y 0 :w -1}
                       {:x 0.5 :y 0.5 :w 0.5}
                       {:x -0.5 :y -0.5 :w -0.5}
                       {:x (rand) :y (rand) :w (rand)}]
        command-length 2000
        data (atom [])]
    (doseq [cmd test-commands]
      (let [state-before @state/robot-pose
            predictions (map #(predict-pose state-before cmd :delta-time %) [1.0 2.0 10 100 1000 2000])
            states-observed (atom [])]
        (add-watch state/robot-pose :check
          (fn [_ _ _ newval]
            (swap! states-observed conj newval)))
        (send-command :motion cmd)
        (Thread/sleep command-length)
        (remove-watch state/robot-pose :check)
        (send-command :motion)
        (swap! data conj [state-before cmd predictions @states-observed])))
    (spit "/home/kirsch/predictions.txt" (with-out-str (pprint @data)))
    @data))
