; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.navigation)

; heuristic: look where you're going

; genutzter Teil des inneren Zustands:
; statevar robot-pose

(def-heuristic look-at-path
               :evaluator (def-expert-fun [alts :goalvar navgoal :optional [params {:max-angle (/ Math/PI 2)}]]
                                          (utmisc/map-to-kv
                                            (fn [proposed-alternative]
                                              (let [[predicted-pose _ motion-direction-angle] (hps.state/predict+ state/robot-pose :command (:value proposed-alternative) :delta-time 1000)]
                                                (util/compare-to-ideal
                                                  {:anglediff (util/delta-radian (:yaw predicted-pose) motion-direction-angle)}
                                                  {:anglediff 0.0}
                                                  {:anglediff 0.0}
                                                  {:anglediff (:max-angle params)})))
                                            alts)))

(def-heuristic look-at-goalpoint
               :evaluator (def-expert-fun [alts :goalvar navgoal :optional [params {:max-angle (/ Math/PI 2)}]]
                                          (utmisc/map-to-kv
                                            (fn [proposed-alternative]
                                              (let [predicted-pose (hps.state/predict state/robot-pose :command (:value proposed-alternative) :delta-time 1000)
                                                    angle-to-goalpoint (util/radian-between-points @state/robot-pose navgoal)]
                                                [(util/compare-to-ideal
                                                   {:anglediff (util/delta-radian (:yaw predicted-pose) angle-to-goalpoint)}
                                                   {:anglediff 0.0}
                                                   {:anglediff 0.0}
                                                   {:anglediff (:max-angle params)})
                                                 {:angle-to-goalpoint angle-to-goalpoint :predicted-yaw (:yaw predicted-pose) :current-yaw (:yaw @state/robot-pose)}]))
                                            alts)))

(def-heuristic velocity
               :evaluator (def-expert-fun [alts :goalvar navgoal]
                                          (utmisc/map-to-kv
                                            (fn [proposed-alternative]
                                              (let [proposed-cmd (second (:value proposed-alternative))
                                                    translational-velocity (/ (math/sqrt (+ (math/expt (:x proposed-cmd) 2) (math/expt (:y proposed-cmd) 2))) (math/sqrt 2)) ; Konstanten sind hier hässlich eingebaut: (sqrt 2) für Translationsgeschw., 1 für Rot.geschw.
                                                    rotational-velocity (/ (Math/abs (:w proposed-cmd)) 1.0)]
                                                (/ (+ translational-velocity (* 0.5 rotational-velocity)) 1.5)))
                                            alts)))

