; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.navigation)

; heuristic: move along topological map

; genutzter Teil des inneren Zustands:
; statevars robot-pose, (people)
; environment topomap

; hier nur ein Producer und ein dummy-evaluator

(def-heuristic a-star
               :producer (def-expert-fun [:goalvar navgoal]
                                         (when navgoal
                                           (let [topomap (:topomap conn/environment*) ; oder aus dem goal holen?
                                                 obstacles (get-static-obstacles)
                                                 topomap-start-point (env/get-closest-reachable-topomap-point @state/robot-pose topomap obstacles) ; 0.3) ; wenn Distanz kleiner als 0.3 ist, gilt der Punkt als erreicht und wird deshalb nicht Teil des Plans
                                                 topomap-goal-point (env/get-closest-reachable-topomap-point navgoal topomap obstacles)]
                                             (when (and topomap-start-point topomap-goal-point) ; eigentlich bräuchte man einen Fallback-Plan, wenn es keine nahen Punkte auf der Topologischen Karte gibt
                                               (list (env/plan-topomap-path topomap-start-point
                                                                            topomap-goal-point
                                                                            topomap))))))
               :evaluator (def-expert-fun [alts] (utmisc/map-to-kv (fn [_] 1.0) alts)))


