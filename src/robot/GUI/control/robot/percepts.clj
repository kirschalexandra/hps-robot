; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.GUI.control.robot)

(defn make-state-elements []
  (let [pose-box (HBox.)
        pose-label (Label. "Robot pose: ")
        pose-content-label (Label.)]

    (gut/add-style pose-label "heading-label")
    (gut/add-style pose-box "control-components-box")

    (letfn [(format-pose [newpose]
              (format "x: %.2f  y: %.2f  az: %.2f" (:x newpose) (:y newpose) (:yaw newpose)))]

      (.setText pose-content-label (format-pose @robot.state/robot-pose))

      (add-watch robot.state/robot-pose :newstate
        (fn [_ _ oldpose newpose]
          (when (not= (dissoc oldpose :timestamp) (dissoc newpose :timestamp))
            (gut/run-in-fx-thread (.setText pose-content-label (format-pose newpose)))))))
            ; (Platform/runLater
            ;   (proxy [Runnable] []
            ;     (run []
            ;       (.setText pose-content-label (format-pose newpose)))))))))

    (gut/add-to-container pose-box pose-label pose-content-label)

    pose-box))