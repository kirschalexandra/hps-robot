; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.GUI.hps.decision-process)

(declare clear-morph-screen)

(defn display-goal-parameters [goaltype]
  (let [paramGrid (GridPane.)
        enter-goal-label (Label. "Enter goal parameters")
        input-fields (conj (map (fn [param]
                                  {:label (Label. (name param))
                                   :input (TextField.)
                                   :param param})
                                (:parameters goaltype))
                           {:label (Label. "timeout") :input (TextField. "15000") :param :timeout}
                           {:label (Label. "log interval") :input (TextField. "200") :param :log-interval})]
    (doseq [row-number (range (count input-fields))]
      (let [row-content (nth input-fields row-number)]
        (.add paramGrid (:label row-content) 0 row-number)
        (.add paramGrid (:input row-content) 1 row-number)))
    (gut/add-style paramGrid "goal-parameters-input-grid")
    [input-fields paramGrid]))

(defn update-result-converter [input-fields]
  (proxy [Callback] []
    (call [^ButtonType b]
      (when-not (= (.getButtonData b) ButtonBar$ButtonData/CANCEL_CLOSE)
        (apply hash-map
               (mapcat
                 (fn [inputfield]
                   (let [textfield-content (.getText (:input inputfield))]
                     (when (seq textfield-content) [(:param inputfield) (read-string textfield-content)])))
                 input-fields))))))


(defn get-goal-parameters []
  (let [dialog (Dialog.)
        dialogpane (.getDialogPane dialog)
        dm-choices (ChoiceBox.)
        available-dms (reverse (dms/list-top-level-dms 'robot.navigation)) ;reverse nur um eine schönere default-Reihenfolge zu haben
        config-choices (ChoiceBox.)
        available-configs (keys @nav/dm-configurations)
        config-default @nav/current-config
        content-box (VBox.)
        parameter-box (VBox.)                               ; Typ egal, ist nur dummy
        achieve-button-type (ButtonType. "Achieve Goal" ButtonBar$ButtonData/OK_DONE)]
    (letfn [(reconfigure-for-goaltype [goaltype]            ;-name]
              (gut/clear-container parameter-box)
              (let [[input-fields paramGrid] (display-goal-parameters goaltype)]
                (gut/add-to-container parameter-box paramGrid)
                (.setResultConverter dialog (update-result-converter input-fields))))
            (reconfigure-for-dm [dm-name]
              (reconfigure-for-goaltype (:goaltype (dms/get-dm-from-name (keyword dm-name) 'robot.navigation))))]

      (.addAll (.getItems dm-choices)
               (into-array String (map name available-dms)))
      (.. dm-choices (getSelectionModel) (selectFirst))

      (.addAll (.getItems config-choices)
               (into-array String (sort (map name available-configs))))
      (.setValue config-choices (name config-default))

      (.setContent dialogpane content-box)
      (gut/add-to-container content-box dm-choices config-choices parameter-box)

      ; default-Wert setzen
      (reconfigure-for-dm (.getValue dm-choices))

      (.add (.getButtonTypes dialogpane) ButtonType/CANCEL)
      (.add (.getButtonTypes dialogpane) achieve-button-type)

      ; bei Änderung des Ziels, Anzeige und Auslesefunktion umkonfigurieren
      (.addListener (.selectedItemProperty (.getSelectionModel dm-choices))
                    (proxy [ChangeListener] []
                      (changed [^ObservableValue ov oldval newval]
                        (reconfigure-for-dm newval))))

      (.setStyle content-box "-fx-padding: 10 10 10 10; -fx-spacing: 10; -fx-alignment: center;")

      (doto dialog
        (.setTitle "Set Goal")
        (.showAndWait))

      [(keyword (.getValue dm-choices)) (keyword (.getValue config-choices)) (.getResult dialog)])))


(defn display-goal-achievement [scene dm-name config goalparams]
  (let [[_ stateRegion] (vec (.getItems (.lookup scene "#hps-centerSplitPane")))
        status-box (VBox.)
        status-label (Label. "Robot running ...")
        progress-bar (ProgressBar.)
        goal-done? (promise)]
    (gut/add-to-container stateRegion status-box)
    (gut/add-to-container status-box status-label progress-bar) ; evtl. einen Button um Prozess abzubrechen
    (gut/add-style status-box "goal-achievement-box")
    (future
      (reset! nav/current-config config)
      (robot.achieve/set-log-interval (:log-interval goalparams))
      (let [trace (robot.achieve/achieve dm-name goalparams)]
        (gut/run-in-fx-thread
          (clear-morph-screen scene)
          (display-decision-process scene (unpack-trace trace)))))))


(defn set-goal [scene]
  (let [[dm-name config goalparams] (get-goal-parameters)]
    (when (seq goalparams) (clear-morph-screen scene) (display-goal-achievement scene dm-name config goalparams))))
