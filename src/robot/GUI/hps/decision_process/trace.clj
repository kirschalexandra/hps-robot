; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.GUI.hps.decision-process)

(defn get-dm-state [dm-name trace trace-index]
  (let [subtrace-for-dm (filter #(= (nth % 2) dm-name) (subvec (vec trace) 0 (inc trace-index)))]
    {:goals   [(nth (last (filter #(= (second %) :goal-set) subtrace-for-dm)) 3)]
              ; goals: aus historischen Gründen einen Vektor mit Zielinhalt zurückliefern, aber nur letztes Ziel berücksichtigen

              ;(reduce (fn [current-goals [_ event-type _ event-data]]
                         ;(case event-type
                           ;:goal-set (conj current-goals event-data)
                           ;(:goal-achieved :goal-failed) (remove #(= % event-data) current-goals)
                           ;current-goals))
                       ;[]
                       ;subtrace-for-dm)
     :setup    (last (filter #(= (second %) :setup) subtrace-for-dm))
     :decision (last (filter #(= (second %) :decision) subtrace-for-dm))}))
    ; letztes Kommando könnte auch noch dabei sein, da muss man aber aufpassen, dass es zur angezeigten decision passt...

(defn get-world-state [trace humans trace-index]
  (loop [[[_ _ source _ :as trace-elem] & trace-rest] (reverse (filter (fn [[_ event-type _ _ ]] (= event-type :state)) (subvec (vec trace) 0 (inc trace-index))))
         robot-state nil
         humans-state ()]
    (if (or (and robot-state (= (count humans-state) (count humans))) (nil? trace-elem))
      {:robot robot-state :humans humans-state}
      (if (nil? source)
        (recur trace-rest (or robot-state trace-elem) humans-state)
        (recur trace-rest robot-state (if (some #(= (nth % 2) source) humans-state) humans-state (conj humans-state trace-elem)))))))

(defn unpack-trace [[trace-header & simple-trace]]
  [trace-header
   (map (fn [[timestamp event source data]] [timestamp event source (logging/expand-type event data)]) simple-trace)])


(defn read-trace-from-file [file]
  (unpack-trace (read-string (slurp file))))
  ; (let [[trace-header & simple-trace] (read-string (slurp file))]
  ;   [trace-header
  ;    (map (fn [[timestamp event source data]] [timestamp event source (logging/expand-type event data)]) simple-trace)]))


;;; --------------------------------------------------------------------- ;;;
;;; Trace and DM statistics                                               ;;;
;;; --------------------------------------------------------------------- ;;;


(defn calculate-trace-timing [trace]
  (let [start (ffirst trace)
        end (first (last trace))]
    {:start start :end end :duration (/ (- end start) 1000.0)}))

(defn calculate-trace-result [trace states]
  (let [[_ goal-status _ goal] (or (some #(when (= (second %) :goal-achieved) %) trace)
                                   (some #(when (= (second %) :goal-failed) %) trace))
        [_ _ _ last-state] (last states)]
    {:result (cond
               (= goal-status :goal-achieved) :success
               (= goal-status :goal-failed) :failure
               :else :unknown)
     :dist-to-goal (rut/point-distance goal last-state)
     :rotation-to-goal (rut/delta-radian (:yaw goal) (:yaw last-state))}))


(defn calculate-derivations [states]
  ;s1 s2 -> s2-neu mit vel, später acc, jerk
  (let [calc-difference (fn [diff-fun v1 v2]
                          (let [timediff-in-s (* (- (:timestamp v2) (:timestamp v1)))]
                            (if (< timediff-in-s 0.001)
                              0
                              (/ (diff-fun v1 v2) timediff-in-s))))
        calculate-velocity (partial calc-difference rut/point-distance)
        calculate-rot-velocity (partial calc-difference #(rut/delta-radian (:yaw %1) (:yaw %2)))
        calculate-acceleration (partial calc-difference #(Math/abs (- (:velocity %2) (:velocity %1))))
        calculate-acceleration-neg (partial calc-difference #(- (:velocity %2) (:velocity %1)))
        calculate-rot-acceleration (partial calc-difference #(Math/abs (- (:rot-velocity %2) (:rot-velocity %1))))
        calculate-jerk (partial calc-difference #(Math/abs (- (:acceleration %2) (:acceleration %1))))
        calculate-rot-jerk (partial calc-difference #(Math/abs (- (:rot-acceleration %2) (:rot-acceleration %1))))]
    (loop [s1 (first states)
           s2 (second states)
           rest-states (rest (rest states))
           result-states []]
      (if s2
        (let [s2-with-vel  (assoc s2 :velocity (calculate-velocity s1 s2)
                                     :rot-velocity (calculate-rot-velocity s1 s2))
              s2-with-acc  (if (:velocity s1)
                             (assoc s2-with-vel
                               :acceleration (calculate-acceleration s1 s2-with-vel)
                               :acceleration-neg (calculate-acceleration-neg s1 s2-with-vel)
                               :rot-acceleration (calculate-rot-acceleration s1 s2-with-vel))
                             s2-with-vel)
              s2-with-jerk (if (:acceleration s1)
                             (assoc s2-with-acc
                               :jerk (calculate-jerk s1 s2-with-acc)
                               :rot-jerk (calculate-rot-jerk s1 s2-with-acc))
                             s2-with-acc)]
          (recur s2-with-jerk (first rest-states) (rest rest-states) (conj result-states s1)))
        (conj result-states s1)))))

(defn get-event-occurrences
  ([trace event] (get-event-occurrences trace event nil))
  ([trace event source]
    (remove #(or (not= (nth % 1) event) (not= (nth % 2) source)) trace)))


(defn count-collisions [poses envname]
  (let [{:keys [inner-walls outer-walls furniture] :as environment}
          (some #(when (= (:name (meta %)) envname) %) (map var-get (vals (ns-publics 'robot.environment))))
        obstacles (concat inner-walls outer-walls (mapcat vals (vals furniture)))
        collisions-per-pose (map (fn [robot-pose]
                                   (some true?
                                     (map (fn [obj-data]
                                            (let [coordinate-limits (env/get-global-coordinate-limits obj-data)]
                                              (some true?
                                                    (map #(rut/point-in-rectangle % coordinate-limits)
                                                         (nav/sample-robot-outline robot-pose conn/PR2))))) ; hier nur PR2 fest drin, könnte man ggf. in trace header übergeben
                                          obstacles)))
                                 poses)]
    (count (filter true? collisions-per-pose))))


(defn count-sideward-backward [poses]
  (loop [count 0
         [p0 p1 & other-poses] poses]
    (if (and p0 p1)
      (let [delta-x (- (:x p1) (:x p0))
            delta-y (- (:y p1) (:x p0))
            velocity-direction-angle (Math/atan2 delta-y delta-x)
            robot-coordinate-angle (- (:yaw p0) velocity-direction-angle)
            velocity-x (* (:velocity p1) (Math/cos robot-coordinate-angle))
            velocity-y (* (:velocity p1) (Math/sin robot-coordinate-angle))]
        (recur
          (if (or (neg? velocity-x) (> (Math/abs velocity-y) velocity-x))
            (inc count)
            count)
          (cons p1 other-poses)))
      count)))

(defn calculate-trace-statistics [trace trace-header]
  (let [states (get-event-occurrences trace :state)
        poses (map #(nth % 3) states)
        pose-derivations (calculate-derivations poses)
        statistics-for-derivation #(mut/calculate-standard-statistics (filter (comp not nil?) (map % pose-derivations)))
        accelerations-neg (filter some? (map :acceleration-neg pose-derivations))
        gamma (apply + (map #(if (neg? %) % 0) accelerations-neg))]
    {:time (calculate-trace-timing trace)
     :result (calculate-trace-result trace states)
     :pose-trace (map #(select-keys % [:x :y :yaw :timestamp]) poses)
     :velocity (statistics-for-derivation :velocity)
     :rot-velocity (statistics-for-derivation :rot-velocity)
     :acceleration (assoc (statistics-for-derivation :acceleration)
                          :deceleration-gamma gamma
                          :deceleration-eta   (- (* -2 (Math/pow gamma 2)) gamma)) ; vielleicht noch über die Länge der Trajektorie skalieren?
     :rot-acceleration (statistics-for-derivation :rot-acceleration)
     :jerk (statistics-for-derivation :jerk)
     :rot-jerk (statistics-for-derivation :rot-jerk)
     :collisions {:number-of-collisions (count-collisions poses (:environment trace-header)) :number-of-states (count poses)}
     :movement {:sideward-backward (count-sideward-backward pose-derivations) :number-of-states (dec (count poses))}}))

(defn count-alternatives [{:keys [decision retained-alternatives rejected-alternatives] :as decision-struct}]
  (let [retained (count retained-alternatives)
        rejected (count rejected-alternatives)
        decision-count (if decision 1 0)
        retained-sorted (sort-by :aggregated-evaluation > retained-alternatives)
        sorted-evaluations (filter identity (map :aggregated-evaluation retained-sorted))]
    {:all (+ retained rejected decision-count) :decision decision-count :retained retained :rejected rejected
     :diff-decision-next (when (>= (count sorted-evaluations) 2)
                           (- (first sorted-evaluations)
                              (second sorted-evaluations)))
     :stdev (mut/stdev (map :aggregated-evaluation retained-alternatives))
     :range (when (not-empty sorted-evaluations) (- (first sorted-evaluations) (last sorted-evaluations)))}))

(defn calculate-statistics-for-bb [trace bb-name]
  (let [internal-decisions (get-event-occurrences trace :decision bb-name)
        executed-decisions (get-event-occurrences trace :command bb-name)
        alternative-counts (map (comp count-alternatives #(nth % 3)) internal-decisions)]
    {:decisions {:internal (count internal-decisions) :executed (count executed-decisions)}
     :alternatives {:number             (mut/calculate-standard-statistics (map :all alternative-counts))
                    :rejected-percent   (mut/calculate-standard-statistics (map #(if (zero? (:all %)) 0 (/ (:rejected %) (:all %))) alternative-counts))
                    :diff-decision-next (mut/calculate-standard-statistics (filter identity (map :diff-decision-next alternative-counts)))
                    :evaluations-stdev  (mut/calculate-standard-statistics (filter identity (map :stdev alternative-counts)))
                    :evaluations-range  (mut/calculate-standard-statistics (filter identity (map :range alternative-counts)))}}))

(defn calculate-bb-statistics [trace trace-header]
  (into {}
        (map #(hash-map % (calculate-statistics-for-bb trace %))
             (:decision-modules trace-header))))



