; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.GUI.hps.drawing)

(defmacro draw-person [gc pose & styling-commands]
  (let [human-radius 0.3]
    `(doto ~gc
      (.save)
      (.beginPath)
      ; position person
      (.translate (:x ~pose) (:y ~pose))
      (.rotate (rut/radian-to-degree (:yaw ~pose)))
      ; person orientation
      (.moveTo ~(- human-radius 0.2) 0)
      (.lineTo ~human-radius 0)
      ; person outline
      (.arc 0 0 ~human-radius ~human-radius 0 360)
      ; close and style
      (.closePath)
      ~@styling-commands
      (.restore))))

(defn draw-human-state [gc pose]
  (draw-person gc pose
    (.setLineWidth 0.05)
    (.setFill (Color/web "cccccc"))
    (.setStroke obstacles-stroke-color)
    (.fill)
    (.stroke)))