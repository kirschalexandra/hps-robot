; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns robot.state
  (:require [clojure.math.numeric-tower :as math]
            [hps.state :as hps :refer :all]))

(defsv robot-pose); :history 5)
(defsv joint-state)
(defsv laser-data) ; Keys: :timestamp :point_list :range_list
;(defsv ella-joint-state)


(def people (atom {}))
; Zugriff auf pose von Person "human":
; @(:human @robot.state/people)

(hps/set-update-frequency 60.0)

;; Modell scheint ganz gut zu passen, evtl. gibt es eine Verzögerung von 2-4 Zeitschritten, das kann aber auch an meiner Messmethode liegen (s. monitoring.clj)
(hps/defmodel robot-pose :motion
  (fn [current-pose navcmd delta-time _]
    (if (empty? navcmd)
      [current-pose 0 0]
      (let [delta-time-in-s (/ delta-time 1000) ; Umrechnung Zeitangabe in s
            dist (* delta-time-in-s (math/sqrt (+ (math/expt (:x navcmd) 2) (math/expt (:y navcmd) 2))))
            movement-angle (+ (:yaw current-pose) (Math/atan2 (:y navcmd) (:x navcmd)))]
        [{:x (+ (:x current-pose) (* (Math/cos movement-angle) dist))
          :y (+ (:y current-pose) (* (Math/sin movement-angle) dist))
          :yaw (+ (:yaw current-pose) (* delta-time-in-s (:w navcmd)))}
         dist
         movement-angle]))))


