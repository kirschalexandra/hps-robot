; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.connection)

;; Connections for commands
(def command-variables (atom nil))
(def service-commands (atom nil))

(defn make-command-service-process [cmd-def]
  (let [cmd-var (atom nil)]
    (add-watch cmd-var :change
      (fn [_ _ _ [newcmd request-id reply-promise]]
        (send-service-request
          request-id
          (:component-name cmd-def)
          (:service-name cmd-def)
          (cl-format nil
            (if (contains? cmd-def :format-string) (:format-string cmd-def) "[~{~s~^, ~}]")
            (vec
              (let [full-cmd-spec (if (contains? cmd-def :command-default)
                                        (merge (:command-default cmd-def) newcmd)
                                        newcmd)]
                (map #(% full-cmd-spec)
                  (filter #(contains? full-cmd-spec %) (:command-order cmd-def)))))))
        (register-reply request-id reply-promise)))
    cmd-var))


(defn make-command-stream-process [cmd-def]
  (let [cmd-var (atom nil :meta {:history '(nil)})]
    (let [cmd-socket (Socket. "localhost" (get-port-for-stream (:stream-name cmd-def)))
          cmd-writer (PrintWriter. (.getOutputStream cmd-socket) true)]
      (add-watch cmd-var :change
        (fn [_ _ _ newcmd]
          (alter-meta! cmd-var #(assoc % :history (cons newcmd (butlast (:history %)))))
          (let [full-cmd-spec (if (contains? cmd-def :command-default)
                                    (merge (:command-default cmd-def) newcmd)
                                    newcmd)]
            (.println cmd-writer (json/write-str full-cmd-spec))))))
    cmd-var))


(defn start-commands
  ([] (start-commands (:commands PR2)))
  ([command-list]
    (swap! command-variables
           merge
           (reduce (fn [hashmap [cmdname cmdparams]]
                     (assoc
                       hashmap
                       cmdname
                       (if (= (:type cmdparams) :service)
                         (make-command-service-process cmdparams)
                         (make-command-stream-process cmdparams))))
                   {}
                   (vec command-list)))
    (swap! service-commands
           union
           (set
             (map first
               (filter (fn [[cmd spec]] (= (:type spec) :service))
                       command-list))))))

(defn stop-commands []
  (run! (fn* [p1__4137058#] (remove-watch (second p1__4137058#) :change)) (vec (deref command-variables))))

(defn send-command
  ([target] (send-command target {}))
  ([target cmd]
    ;(println "send cmd: " cmd " to target: " target)
    (if (contains? @service-commands target)
      (let [request-id (gensym)
            reply (promise)]
        (reset! (target @command-variables) (list cmd request-id reply))
        @reply)
      (reset! (target @command-variables) cmd))))
