; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.connection)


;; Define robots
;; Wertebereiche und Erklärung der Armgelenke s. ~/arbeit/docs/manuals/projects/robot/joint-limits.txt
(def PR2
  {:percepts {:pose        {:var state/robot-pose  :stream-name "robot.pose"}
              :joint-state {:var state/joint-state :stream-name "robot.joint_state"}
              :lasser-data {:var state/laser-data  :stream-name "robot.laserscanner"}}
   :commands {:motion {:type :stream :stream-name "robot.motion"
                       :command-default {:x 0.0 :y 0.0 :w 0.0}
                       :max-values {:x 1.0 :y 1.0 :w 1.0}}
              :torso  {:type :service :component-name "robot.torso"
                       :service-name "set_translation" :command-order [:joint :value]
                       :command-default {:joint "torso_lift_joint"}}
              :head   {:type :service :component-name "robot.torso.head"
                       :service-name "set_rotations" :command-order [:pan :tilt]
                       :command-default {:pan 0.0 :tilt 0.0}
                       :format-string "[[~{~f~^, ~}]]"}
;; Befehle zum Bewegen der Arme:
;; move-...-arm-joint: Bewegt ein Gelenk mit gegebener Geschwindigkeit (wenn nicht angegeben, wird default-Geschwindigkeit in Morse bestimmt)
;; set-...-arm-joint: Stellt ein Gelenk auf gegebenen Winkel ein ohne Bewegung durchzuführen
;; set-...-arm-joints: Stellt alle Gelenke auf die gegebenen Winkel ein, nicht angegebene Winkel werden auf 0 gestellt
;; -> Zurücksetzen aller Gelenke einfach mit (send-command :set-...-arm-joints {})
;; -> set-Befehle sind schneller, daher auch für tuck-arms verwendet, nur wenn es hübsch aussehen muss, move-Befehle verwenden
              :move-right-arm-joint  {:type :service :component-name "robot.torso.r_arm"
                                      :service-name "rotate" :command-order [:joint :value :speed]} ; :speed ist optional
              :set-right-arm-joint   {:type :service :component-name "robot.torso.r_arm"
                                      :service-name "set_rotation" :command-order [:joint :value]}
              :set-right-arm-joints  {:type :service :component-name "robot.torso.r_arm"
                                      :service-name "set_rotations"
                                      :command-order [:r-shoulder-pan-joint :r-shoulder-lift-joint :r-upper-arm-roll-joint :r-elbow-flex-joint
                                                      :r-forearm-roll-joint :r-wrist-flex-joint :r-wrist-roll-joint]
                                      :command-default {:r-elbow-flex-joint 0.0 :r-shoulder-lift-joint 0.0 :r-upper-arm-roll-joint 0.0
                                                        :r-wrist-roll-joint 0.0 :r-shoulder-pan-joint 0.0 :r-forearm-roll-joint 0.0 :r-wrist-flex-joint 0.0}
                                      :format-string "[[~{~f~^, ~}]]"}
              :move-left-arm-joint   {:type :service :component-name "robot.torso.l_arm"
                                      :service-name "rotate" :command-order [:joint :value :speed]}
              :set-left-arm-joint    {:type :service :component-name "robot.torso.l_arm"
                                      :service-name "set_rotation" :command-order [:joint :value]}
              :set-left-arm-joints   {:type :service :component-name "robot.torso.l_arm"
                                      :service-name "set_rotations"
                                      :command-order [:l-shoulder-pan-joint :l-shoulder-lift-joint :l-upper-arm-roll-joint :l-elbow-flex-joint
                                                      :l-forearm-roll-joint :l-wrist-flex-joint :l-wrist-roll-joint]
                                      :command-default {:l-elbow-flex-joint 0.0 :l-shoulder-lift-joint 0.0 :l-upper-arm-roll-joint 0.0
                                                        :l-wrist-roll-joint 0.0 :l-shoulder-pan-joint 0.0 :l-forearm-roll-joint 0.0 :l-wrist-flex-joint 0.0}
                                      :format-string "[[~{~f~^, ~}]]"}
              :right-gripper-grab    {:type :service :component-name "robot.torso.r_arm.r_gripper"
                                      :service-name "grab" :command-default {}}
              :right-gripper-release {:type :service :component-name "robot.torso.r_arm.r_gripper"
                                      :service-name "release" :command-default {}}
              :left-gripper-grab     {:type :service :component-name "robot.torso.l_arm.l_gripper"
                                      :service-name "grab" :command-default {}}
              :left-gripper-release  {:type :service :component-name "robot.torso.l_arm.l_gripper"
                                      :service-name "release" :command-default {}}}
    :constants {:half-width 0.355 :half-depth 0.33}})

(def pr2-joint-limits-left {:l-shoulder-pan-joint [-2.28 0.7]
                            :l-shoulder-lift-joint [-0.5 1.39]
                            :l-upper-arm-roll-joint [-2.3 2.3]
                            :l-elbow-flex-joint [0 2.3]
                            :l-forearm-roll-joint [(- Math/PI) Math/PI]
                            :l-wrist-roll-joint [(- Math/PI) Math/PI]
                            :l-wrist-flex-joint [0 2.18]})

(def pr2-joint-limits-right {:r-shoulder-pan-joint [-2.28 0.7]
                             :r-shoulder-lift-joint [-0.5 1.39]
                             :r-upper-arm-roll-joint [-2.3 2.3]
                             :r-elbow-flex-joint [0 2.3]
                             :r-forearm-roll-joint [(- Math/PI) Math/PI]
                             :r-wrist-roll-joint [(- Math/PI) Math/PI]
                             :r-wrist-flex-joint [0 2.18]})

; (def pr2-velocity-limits {:x 1.0 :y 1.0 :w 1.0})

(def ATRV
  {:commands {:motion {:type :service :component-name "atrv.motion"
                       :service-name "set_speed" :command-order [:x :y]}}
   :constants {:half-width 0.355 :half-depth 0.33}}) ; keine Ahnung wie groß der ATRV ist, nur damit hier irgendwas steht
