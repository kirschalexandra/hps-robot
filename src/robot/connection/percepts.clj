; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(in-ns 'robot.connection)

;; Connections for percepts
;; Positionsdaten kommen ca. alle 16-17ms -> 60 Hz
(let [percepts-active? (atom false)]

  (defn start-percepts
    ([] (start-percepts (:percepts PR2)))
    ([percepts-list]
      (reset! percepts-active? true)
      (run! (fn* [p1__4169596#] (let [[percept-name percept-def] p1__4169596#] (future (let [percept-socket (Socket. "localhost" (get-port-for-stream (:stream-name percept-def))) percept-scan (Scanner. (BufferedReader. (InputStreamReader. (.getInputStream percept-socket))))] (.useDelimiter percept-scan "\n") (while (deref percepts-active?) (when (.hasNext percept-scan) (reset! (:var percept-def) (json/read-str (.next percept-scan) :key-fn keyword))) (Thread/sleep 15)) (.close percept-socket))))) (vec percepts-list))))

  (defn stop-percepts []
    (reset! percepts-active? false))

)
