; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns robot.connection
  (:require [clojure.data.json :as json]
            [robot.state :as state]
            [clojure.string :as str]
            [robot.environment :as env])
  (:import (java.net Socket SocketException)
           (java.io OutputStreamWriter PrintWriter InputStreamReader BufferedReader)
           (java.util Scanner))
  (:use [clojure.pprint] [clojure.set])
  (:load "connection/robot-definitions" "connection/service-sockets" "connection/commands" "connection/percepts" "connection/avatars"))

(declare robot*)
(declare environment*)


;; Start and stop all connections for a robot
(defn start-robot
  ([] (start-robot PR2))
  ([robot-def]
    (start-service-socket)
    (future (watch-service-replies))
    (let [sim-info (clojure.data.json/read-str (second (invoke-service-with-reply "simulation" "details" [])))
          agents (get sim-info "robots")
          humans (map (fn [{name "name" components "components"}]
                        [name (key (first (filter #(= (get (val %) "type") "Pose") components)))]) ; ich gehe davon aus, dass nur ein Stream eine Pose sendet
                      (filter #(= "Human" (get % "type")) agents))]
      ; globale Definition von Umgebung und genutztem Roboter
      (def environment* (var-get (get (ns-interns (find-ns 'robot.environment)) (symbol (last (clojure.string/split (get sim-info "environment") #"/"))))))
      ; (def environment* (eval (symbol (str "robot.environment/" (last (clojure.string/split (get sim-info "environment") #"/"))))))
      (def robot* (var-get (get (ns-interns (find-ns 'robot.connection))
                                (symbol (get (first (filter #(not (= "Human" (get % "type"))) agents)) "type")))))
      ; start robot percepts
      (start-percepts (:percepts robot*))
      ; start human percepts
      (doseq [[human-name stream-name] humans]
        (let [posevar (atom nil :meta {:name (symbol human-name)})]
          (swap! state/people assoc (keyword human-name) posevar)
          (start-percepts {:pose {:var posevar :stream-name stream-name}}))))
    (start-commands (:commands robot*))))

(defn stop-robot
  ([] (stop-robot true))
  ([stop-simulator?]
    (stop-percepts)
    (stop-commands)
    (stop-watching-for-service-replies)
    (when stop-simulator?
      (send-service-request "simulation" "quit" []))))


